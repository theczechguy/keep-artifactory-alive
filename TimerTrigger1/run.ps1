# Input bindings are passed in via param block.
param($Timer)

# Get the current universal time in the default string format.
$currentUTCtime = (Get-Date).ToUniversalTime()

# The 'IsPastDue' property is 'true' when the current function invocation is later than scheduled.
if ($Timer.IsPastDue) {
    Write-Host "PowerShell timer is running late!"
}

# Write an information log with the current time.
Write-Host "PowerShell timer trigger function ran! TIME: $currentUTCtime"

#configure artifactory connection
Write-Information 'Getting values from keyvault "artifactory"'
$artifactoryPass = get-azkeyvaultsecret -vaultname 'artifactory' -secretname 'artifactoryPass' -AsPlainText
$artifactoryUsername = get-azkeyvaultsecret -vaultname 'artifactory' -secretname 'artifactoryUser' -AsPlainText
[securestring]$secStringPassword = ConvertTo-SecureString $artifactoryPass -AsPlainText -Force
[pscredential]$credObject = New-Object System.Management.Automation.PSCredential ($artifactoryUsername, $secStringPassword)

$fileName = 'alive_{0}.txt' -f (get-date -Format 'yyyy-MM-dd-hh-mm-ss')
Write-Information "New file will be: $fileName"
Invoke-RestMethod -Method put -Uri "https://theczechguy.jfrog.io/artifactory/keep_alive/$fileName" -Credential $credObject -Body "I keep rolin"